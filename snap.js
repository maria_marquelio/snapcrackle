
function snapCrackle(maxValue) {
    let aux=[]; 
    for (let i=1;i<=maxValue;i++){
        if (ehImpar(i) === true && ehMult(i) === true){
            aux[i-1]='SnapCrackle';
        }
        else if (ehImpar(i) === true){
            aux[i-1]='Snap'; 
        }
        else if (ehMult(i) === true){
            aux[i-1]='Crackle'; 
        }
        else {
            aux[i-1]=i;
        }    
    }
    return 'SnapCrackle => ' + aux;
  }

  document.write(snapCrackle(15));

  function snapCracklePrime(maxValue) {
    let aux=[]; 
    for (let i=1;i<=maxValue;i++){

        if(ehImpar(i) === true && ehMult(i) === true && ehPrimo(i) === true) {
            aux[i-1]='SnapCracklePrime';
        } else if (ehImpar(i) === true && ehMult(i) === true){
            aux[i-1]='SnapCrackle';
        }
        else if (ehImpar(i) === true && (ehPrimo(i) === true)){
            aux[i-1]='SnapPrime'; 
        }else if (ehImpar(i) === true) {
            aux[i-1]='Snap';
        }
        else if (ehMult(i) === true && (ehPrimo(i) === true)){
            aux[i-1]='CracklePrime'; 
        }else if (ehMult(i) === true){
            aux[i-1]='Crackle';
        }else if(ehPrimo(i) === true) {
            aux[i-1]='Prime';
        }else {
            aux[i-1]=i;
        }    
    }
    return '<br> <br> SnapCracklePrime => ' + aux;
  }

  document.write(snapCracklePrime(15));

  function ehPrimo(num) {
    if (num === 2){
        return true;
    }  
    for (let i = 2; i < num; i++) {
        if (num % i === 0){
            return false;         
    }
    return true;
}   
  }

  function ehImpar(num){
    
    if (num % 2 === 1){
      return true;   
    }
    return false;
  }

  function ehMult(num){
    
    if (num % 5 === 0){
      return true;   
    }
    return false;
  }
